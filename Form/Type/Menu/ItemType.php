<?php

namespace Nitra\MenuBundle\Form\Type\Menu;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ItemType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('translations', 'nl_translation', array(
            'fields'        => array(
                'title'         => array(
                    'label'         => 'item.title.label',
                    'help'          => 'item.title.help',
                ),
                'description'   => array(
                    'label'         => 'item.description.label',
                    'help'          => 'item.description.help',
                    'required'      => false,
                ),
            ),
        ));
        $builder->add('isActive', 'checkbox', array(
            'label'         => 'item.is_active.label',
            'help'          => 'item.is_active.help',
            'required'      => false,
        ));
        $builder->add('type', 'choice', array(
            'label'         => 'item.type.label',
            'help'          => 'item.type.help',
            'choices'       => $options['type'],
            'attr'          => array(
                'onchange'      => 'getTypeContent($(this));',
            ),
        ));
        $builder->add('typeContent_url', 'text', array(
            'label'         => 'item.type_content_url.label',
            'help'          => 'item.type_content_url.help',
            'required'      => false,
        ));
        $builder->add('typeContent_category', 'text', array(
            'label'         => 'item.type_content_category.label',
            'help'          => 'item.type_content_category.help',
            'data'          => $options['typeContent_category'],
            'required'      => false,
            'attr'          => array(
                'style'         => 'min-width: 200px',
            ),
        ));
        $builder->add('typeContent_information', 'text', array(
            'label'         => 'item.type_content_information.label',
            'help'          => 'item.type_content_information.help',
            'data'          => $options['typeContent_information'],
            'required'      => false,
            'attr'          => array(
                'style'         => 'min-width: 200px',
            ),
        ));
        $builder->add('typeContent_infoCategory', 'text', array(
            'label'         => 'item.type_content_information_category.label',
            'help'          => 'item.type_content_information_category.help',
            'data'          => $options['typeContent_infoCategory'],
            'required'      => false,
            'attr'          => array(
                'style'         => 'min-width: 200px',
            ),
        ));
        $builder->add('typeContent_product', 'text', array(
            'label'         => 'item.type_content_product.label',
            'help'          => 'item.type_content_product.help',
            'data'          => $options['typeContent_product'],
            'required'      => false,
            'attr'          => array(
                'style'         => 'min-width: 200px',
            ),
        ));
        $builder->add('typeContent_badge', 'text', array(
            'label'         => 'item.type_content_badge.label',
            'help'          => 'item.type_content_badge.help',
            'data'          => $options['typeContent_badge'],
            'required'      => false,
            'attr'          => array(
                'style'         => 'min-width: 200px',
            ),
        ));
        $builder->add('trans_typeContent_html', 'nl_translation', array(
            'label'         => 'item.type_content_html.label',
            'help'          => 'item.type_content_html.help',
            'fields'        => array(
                'typeContent_html'         => array(
                    'label'         => ' ',
                    'type'          => 'ckeditor',
                    'required'      => false,
                ),
            ),
        ));
        $builder->add('parent', 'choice', array(
            'label'         => 'item.parent.label',
            'help'          => 'item.parent.help',
            'choices'       => $options['parents'],
        ));
        $builder->add('image', 'nlupload', array(
            'label'         => 'item.image.label',
            'help'          => 'item.image.help',
            'multiple'      => false,
            'base_path'     => 'images/menu',
            'required'      => false,
        ));
        $builder->add('class', 'text', array(
            'label'         => 'item.class.label',
            'help'          => 'item.class.help',
            'required'      => false,
        ));
        $builder->add('showTitle', 'checkbox', array(
            'label'         => 'item.show_title.label',
            'help'          => 'item.show_title.help',
            'required'      => false,
        ));
        $builder->add('isGroup', 'checkbox', array(
            'label'         => 'item.is_group.label',
            'help'          => 'item.is_group.help',
            'required'      => false,
        ));
        $builder->add('columns', 'text', array(
            'label'         => 'item.columns.label',
            'help'          => 'item.columns.help',
            'required'      => false,
        ));
//        $builder->add('detailColumnsWidth', 'textarea', array(
//            'label'         => 'item.detail_columns_width.label',
//            'help'          => 'item.detail_columns_width.help',
//            'required'      => false,
//        ));
        $builder->add('submenuType', 'choice', array(
            'label'         => 'item.submenu_type.label',
            'help'          => 'item.submenu_type.help',
            'choices'       => $options['submenuType'],
            'attr'          => array(
                'onchange'      => 'getSubmenuTypeContent($(this));',
            ),
        ));
        $builder->add('trans_submenuContent_html', 'nl_translation', array(
            'label'         => 'item.submenu_content_html.label',
            'help'          => 'item.submenu_content_html.help',
            'fields'        => array(
                'submenuContent_html'         => array(
                    'label'         => ' ',
                    'type'          => 'ckeditor',
                    'required'      => false,
                ),
            ),
        ));
        $builder->add('id', 'hidden');
        $builder->add('save', 'button', array(
            'label'         => 'item.save.label',
            'attr'          => array(
                'onclick'       => 'saveItem();',
            ),
        ));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'                => 'Nitra\MenuBundle\Document\Item',
            'parents'                   => array(), 
            'type'                      => array(
                'url'                       => 'item.type.choices.url',
                'category'                  => 'item.type.choices.category',
                'product'                   => 'item.type.choices.product',
                'badge'                     => 'item.type.choices.badge',
                'infoCategory'              => 'item.type.choices.infoCategory',
                'information'               => 'item.type.choices.information',
                'html'                      => 'item.type.choices.html',
            ),
            'submenuType'               => array(
                'menu'                      => 'menu.word',
                'html'                      => 'html',
            ),
            'typeContent_category'              => null,
            'typeContent_information'           => null,
            'typeContent_infoCategory'          => null,
            'typeContent_product'               => null,
            'typeContent_badge'                 => null,
            'translation_domain'                => 'NitraMenuBundle',
        ));
    }

    public function getName()
    {
        return 'item';
    }
}