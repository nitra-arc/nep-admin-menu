<?php

namespace Nitra\MenuBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document
 */
class Menu
{
    /**
     * @ODM\Id(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ODM\Boolean
     */
    private $isActive;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    private $store;
    
    /**
     * @ODM\EmbedMany(targetDocument="Item")
     */
    private $items;
    
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set store
     *
     * @param Nitra\StoreBundle\Document\Store $store
     * @return self
     */
    public function setStore($store)
    {
        $this->store = $store;
        return $this;
    }

    /**
     * Get store
     *
     * @return Nitra\StoreBundle\Document\Store $store
     */
    public function getStore()
    {
        return $this->store;
    }
    
    /**
     * Add menuItem
     *
     * @param Nitra\MenuBundle\Document\Item $item
     */
    public function addItem(\Nitra\MenuBundle\Document\Item $item)
    {
        $this->items[] = $item;
    }

    /**
     * Remove menuItem
     *
     * @param Nitra\MenuBundle\Document\Item $item
     */
    public function removeItem(\Nitra\MenuBundle\Document\Item $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get menuItem
     *
     * @return Doctrine\Common\Collections\Collection $items
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set menuItem
     *
     * @return Doctrine\Common\Collections\Collection $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }
}