<?php

namespace Nitra\MenuBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\EmbeddedDocument 
 */
class Item
{
    
    use \Nitra\StoreBundle\Traits\LocaleDocument;
    /**
     * @ODM\Id(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     * @Gedmo\Translatable
     */
    protected $title;
    
    /**
     * @ODM\String
     * @Gedmo\Translatable
     */
    protected $description;
    
    /**
     * @ODM\Boolean
     */
    private $isActive;
    
    /**
     * @ODM\String
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $type;
    
    /**
     * @ODM\String
     */
    private $image;
    
    /**
     * @ODM\String
     * @Assert\Length(max="255")
     */
    private $class;
    
    /**
     * @ODM\Boolean
     */
    private $showTitle;
    
    /**
     * @ODM\Boolean
     */
    private $isGroup;
    
    /**
     * @ODM\Int
     * @Assert\Length(max="11")
     */
    private $columns;
    
    /**
     * @ODM\String
     */
    private $detailColumnsWidth;
    
    /**
     * @ODM\String
     * @Assert\Length(max="255")
     */
    private $submenuType;
    
    /**
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $submenuContent_html;
    
    /**
     * @ODM\String
     */
    private $parent;
    
    /**
     * @ODM\String
     */
    private $typeContent_url;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Category")
     */
    private $typeContent_category;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\InformationBundle\Document\Information")
     */
    private $typeContent_information;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\InformationBundle\Document\InformationCategory")
     */
    private $typeContent_infoCategory;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Product")
     */
    private $typeContent_product;
    
    /**
     * @ODM\ReferenceOne(targetDocument="Nitra\ProductBundle\Document\Badge")
     */
    private $typeContent_badge;
    
    /**
     * @ODM\String
     * @Gedmo\Translatable
     */
    private $typeContent_html;
    
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set title
     *
     * @param string $title
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return self
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean $isActive
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return self
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * Get image
     *
     * @return string $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return self
     */
    public function setClass($class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * Get class
     *
     * @return string $class
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set showTitle
     *
     * @param boolean $showTitle
     * @return self
     */
    public function setShowTitle($showTitle)
    {
        $this->showTitle = $showTitle;
        return $this;
    }

    /**
     * Get showTitle
     *
     * @return boolean $showTitle
     */
    public function getShowTitle()
    {
        return $this->showTitle;
    }

    /**
     * Set isGroup
     *
     * @param boolean $isGroup
     * @return self
     */
    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;
        return $this;
    }

    /**
     * Get isGroup
     *
     * @return boolean $isGroup
     */
    public function getIsGroup()
    {
        return $this->isGroup;
    }

    /**
     * Set columns
     *
     * @param int $columns
     * @return self
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
        return $this;
    }

    /**
     * Get columns
     *
     * @return int $columns
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Set detailColumnsWidth
     *
     * @param string $detailColumnsWidth
     * @return self
     */
    public function setDetailColumnsWidth($detailColumnsWidth)
    {
        $this->detailColumnsWidth = $detailColumnsWidth;
        return $this;
    }

    /**
     * Get detailColumnsWidth
     *
     * @return string $detailColumnsWidth
     */
    public function getDetailColumnsWidth()
    {
        return $this->detailColumnsWidth;
    }

    /**
     * Set submenuType
     *
     * @param string $submenuType
     * @return self
     */
    public function setSubmenuType($submenuType)
    {
        $this->submenuType = $submenuType;
        return $this;
    }

    /**
     * Get submenuType
     *
     * @return string $submenuType
     */
    public function getSubmenuType()
    {
        return $this->submenuType;
    }

    /**
     * Set parent
     *
     * @param string $parent
     * @return self
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Get parent
     *
     * @return string $parent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set typeContentCategory
     *
     * @param Nitra\ProductBundle\Document\Category $typeContentCategory
     * @return self
     */
    public function setTypeContentCategory($typeContentCategory)
    {
        $this->typeContent_category = $typeContentCategory;
        return $this;
    }

    /**
     * Get typeContentCategory
     *
     * @return Nitra\ProductBundle\Document\Category $typeContentCategory
     */
    public function getTypeContentCategory()
    {
        return $this->typeContent_category;
    }

    /**
     * Set typeContentUrl
     *
     * @param string $typeContentUrl
     * @return self
     */
    public function setTypeContentUrl($typeContentUrl)
    {
        $this->typeContent_url = $typeContentUrl;
        return $this;
    }

    /**
     * Get typeContentUrl
     *
     * @return string $typeContentUrl
     */
    public function getTypeContentUrl()
    {
        return $this->typeContent_url;
    }

    /**
     * Set typeContentInformation
     *
     * @param Nitra\InformationBundle\Document\Information $typeContentInformation
     * @return self
     */
    public function setTypeContentInformation($typeContentInformation)
    {
        $this->typeContent_information = $typeContentInformation;
        return $this;
    }

    /**
     * Get typeContentInformation
     *
     * @return Nitra\InformationBundle\Document\Information $typeContentInformation
     */
    public function getTypeContentInformation()
    {
        return $this->typeContent_information;
    }
    
     /**
     * Set typeContentInformationCategory
     * @param Nitra\InformationBundle\Document\InformationCategory $typeContentInformationCategory
     * @return self
     */
    public function setTypeContentInfoCategory($typeContentInformationCategory)
    {
        $this->typeContent_infoCategory = $typeContentInformationCategory;
        return $this;
    }

    /**
     * Get typeContentInformationCategory
     * @return Nitra\InformationBundle\Document\InformationCategory $typeContentInformationCategory
     */
    public function getTypeContentInfoCategory()
    {
        return $this->typeContent_infoCategory;
    }

    /**
     * Set typeContentProduct
     *
     * @param Nitra\ProductBundle\Document\Product $typeContentProduct
     * @return self
     */
    public function setTypeContentProduct($typeContentProduct)
    {
        $this->typeContent_product = $typeContentProduct;
        return $this;
    }

    /**
     * Get typeContentProduct
     *
     * @return Nitra\ProductBundle\Document\Product $typeContentProduct
     */
    public function getTypeContentProduct()
    {
        return $this->typeContent_product;
    }

    /**
     * Set typeContentHtml
     *
     * @param string $typeContentHtml
     * @return self
     */
    public function setTypeContentHtml($typeContentHtml)
    {
        $this->typeContent_html = $typeContentHtml;
        return $this;
    }

    /**
     * Get typeContentHtml
     *
     * @return string $typeContentHtml
     */
    public function getTypeContentHtml()
    {
        return $this->typeContent_html;
    }

    /**
     * Set submenuContentHtml
     *
     * @param string $submenuContentHtml
     * @return self
     */
    public function setSubmenuContentHtml($submenuContentHtml)
    {
        $this->submenuContent_html = $submenuContentHtml;
        return $this;
    }

    /**
     * Get submenuContentHtml
     *
     * @return string $submenuContentHtml
     */
    public function getSubmenuContentHtml()
    {
        return $this->submenuContent_html;
    }

    /**
     * Set typeContentBadge
     *
     * @param Nitra\ProductBundle\Document\Badge $typeContentBadge
     * @return self
     */
    public function setTypeContentBadge($typeContentBadge)
    {
        $this->typeContent_badge = $typeContentBadge;
        return $this;
    }

    /**
     * Get typeContentBadge
     *
     * @return Nitra\ProductBundle\Document\Badge $typeContentBadge
     */
    public function getTypeContentBadge()
    {
        return $this->typeContent_badge;
    }
}