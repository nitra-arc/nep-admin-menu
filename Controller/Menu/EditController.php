<?php

namespace Nitra\MenuBundle\Controller\Menu;

use Admingenerated\NitraMenuBundle\BaseMenuController\EditController as BaseEditController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Nitra\MenuBundle\Form\Type\Menu\ItemType;
use Nitra\MenuBundle\Document\Item;

class EditController extends BaseEditController
{
    /**
     * Update tree menu
     * @Route("/tree_update/{menuId}", name="Nitra_MenuBundle_Menu_treeUpdate")
     */
    public function treeUpdate($menuId)
    {
        $Menu = $this->getObject($menuId);

        if (!$Menu) {
            throw new NotFoundHttpException("The Nitra\MenuBundle\Document\Menu with id $menuId can't be found");
        }

        try {
            $data = $this->getRequest()->get('list');

            $items = array();
            foreach ($data as $childId => $parentId) {
                foreach ($Menu->getItems() as $item) {
                    if ($item->getId() == $childId) {
                        $item->setParent($parentId);
                        $items[] = $item;
                    }
                }
            }
            $Menu->setItems($items);
            $this->saveObject($Menu);

            return $this->render('NitraMenuBundle:TreeMenu:index.html.twig', array(
                "Menu"  => $Menu,
            ));
        } catch (\Exception $e) {
            return new Response('', 400);
        }
    }

    /**
     * Delete menu item
     * @Route("/delete_item/{menuId}", name="Nitra_MenuBundle_Menu_itemDelete")
     */
    public function itemDelete($menuId)
    {
        $Menu = $this->getObject($menuId);

        if (!$Menu) {
            throw new NotFoundHttpException("The Nitra\MenuBundle\Document\Menu with id $menuId can't be found");
        }

        try {
            $itemId = $this->getRequest()->get('itemId');

            foreach ($Menu->getItems() as $item) {
                if ($item->getId() == $itemId) {
                    $Menu->removeItem($item);
                }
            }

            $this->saveObject($Menu);

            return $this->render('NitraMenuBundle:TreeMenu:index.html.twig', array(
                "Menu"  => $Menu,
            ));
        } catch (\Exception $e) {
            return new Response('', 400);
        }
    }

    /**
     * Edit menu item
     * @Route("/get_item/{menuId}", name="Nitra_MenuBundle_Menu_itemGet")
     */
    public function itemGet($menuId)
    {
        $Menu = $this->getObject($menuId);

        if (!$Menu) {
            throw new NotFoundHttpException("The Nitra\MenuBundle\Document\Menu with id $menuId can't be found");
        }

        try {
            $itemId = $this->getRequest()->get('itemId');

            $editItem = NULL;
            foreach ($Menu->getItems() as $item) {
                if ($item->getId() == $itemId) {
                    $editItem = $item;
                }
            }

            if (!isset($editItem)) {
                $editItem = new Item();
            }

            $parents = array(
                'null'  => 'ROOT',
            );
            $this->getParents($Menu, $parents, $editItem);

            $options = array(
                'parents'                           => $parents,
                'typeContent_category'              => $editItem->getTypeContentCategory()              ? $editItem->getTypeContentCategory()->getId()              : '',
                'typeContent_information'           => $editItem->getTypeContentInformation()           ? $editItem->getTypeContentInformation()->getId()           : '',
                'typeContent_infoCategory'          => $editItem->getTypeContentInfoCategory()          ? $editItem->getTypeContentInfoCategory()->getId()          : '',
                'typeContent_product'               => $editItem->getTypeContentProduct()               ? $editItem->getTypeContentProduct()->getId()               : '',
                'typeContent_badge'                 => $editItem->getTypeContentBadge()                 ? $editItem->getTypeContentBadge()->getId()                 : '',
            );

            $itemType = new ItemType();

            $form = $this->createForm($itemType, $editItem, $options);

            return $this->render('NitraMenuBundle:Item:form.html.twig', array(
                "Item"  => $editItem,
                "Menu"  => $Menu,
                "form"  => isset($form) ? $form->createView() : '',
            ));
        } catch (\Exception $e) {
            return new Response('', 400);
        }
    }

    /**
     * Save menu item
     * @Route("/save_item/{menuId}", name="Nitra_MenuBundle_Menu_itemSave")
     */
    public function itemSave($menuId)
    {
        $Menu = $this->getObject($menuId);

        if (!$Menu) {
            throw new NotFoundHttpException("The Nitra\MenuBundle\Document\Menu with id $menuId can't be found");
        }
        // Получаем request
        $newItem = $this->getRequest()->get('item');
        
        $item = null;
		// находим item меню
        foreach($Menu->getItems() as $i) {
            if($i->getId() == $newItem['id']) {
                $item = $i;
                break;
            }
        }
        // если item существует берем его иначе сздаем новый
        $object = $item ? $item : new Item();

        // биндим форму
        $form = $this->createForm(new ItemType(), $object);
        $form->bind($newItem);

        // устанавливаем тип меню
        if($newItem['typeContent_category']) {
            $category    = $this->getDocumentManager()->getRepository('NitraProductBundle:Category')->find($newItem['typeContent_category']);
            if ($category) { $object->setTypeContentCategory($category); }
        } else if($newItem['typeContent_information']) {
            $information = $this->getDocumentManager()->getRepository('NitraInformationBundle:Information')->find($newItem['typeContent_information']);
            if ($information) { $object->setTypeContentInformation($information); }
        } else if($newItem['typeContent_infoCategory']) {
            $informationCategory = $this->getDocumentManager()->getRepository('NitraInformationBundle:InformationCategory')->find($newItem['typeContent_infoCategory']);
            if ($informationCategory) { $object->setTypeContentInfoCategory($informationCategory); }
        }else if($newItem['typeContent_product']) {
            $product     = $this->getDocumentManager()->getRepository('NitraProductBundle:Product')->find($newItem['typeContent_product']);
            if ($product) { $object->setTypeContentProduct($product); }
        } else if($newItem['typeContent_badge']) {
            $badge       = $this->getDocumentManager()->getRepository('NitraProductBundle:Badge')->find($newItem['typeContent_badge']);
            if ($badge) { $object->setTypeContentBadge($badge); }
        }
        // добавляем родителя item
        $object->setParent($newItem['parent']);
        // если новый обьект item добавляем в меню
        if(!$item) {
            $Menu->addItem($object);
        }
        
        $this->saveObject($Menu);

        return $this->render('NitraMenuBundle:TreeMenu:index.html.twig', array(
            "Menu" => $Menu,
        ));

    }
    
    protected function getRealItemValue(&$field, $val)
    {
        if (is_array($val)) {
            $locale = $this->container->getParameter('locale');
            if (key_exists($locale, $val)) {
                return $this->getRealItemValue($field, $val[$locale]);
            } else {
                $field = array_keys($val)[0];
                return array_values($val)[0];
            }
        }
        
        return $val;
    }

    /**
     * Get autocomplete for ajax query
     * @Route("/ajax_get_autocomplete", name="Nitra_MenuBundle_Menu_ajaxGetAutocomplete")
     */
    public function ajaxGetAutocomplete(Request $request)
    {
        $q                  = $request->get('q');
        $page               = $request->get('page');
        $limit              = $request->get('limit');
        $document           = $request->get('document');
        $autocompleteReturn = array();

        $words = preg_split('/\s+/', trim($q));

        $query = $this->getQueryAutocomplite($document, $words, $page, $limit);

        $autocompleteReturn['results'] = array();
        foreach ($query as $val) {
            $autocompleteReturn['results'][] = array(
                'id'    => $val->getId(),
                'text'  => $val->getName(),
            );
        }
        $autocompleteReturn['total'] = count($query);

        return new JsonResponse($autocompleteReturn);
    }

    /**
     * Get query for autocomplite
     */
    protected function getQueryAutocomplite($document, $words, $page = 1, $limit = 10)
    {
        return $this->getDocumentManager()->createQueryBuilder($document)
            ->field('name')->equals(new \MongoRegex('/(' . implode('|', $words) . ')/i'))
            ->sort('name')
            ->skip(($page - 1) * $limit)
            ->limit($limit)
            ->getQuery()
            ->execute();
    }

    /**
     * Get array of all menu items
     */
    protected function getParents($Menu, &$parents, $exclude, $parent = 'null', $level = '- ')
    {
        foreach ($Menu->getItems() as $item) {
            if ($item->getParent() == $parent) {
                if ((!isset($exclude)) || (isset($exclude) && $item->getId() != $exclude->getId())) {
                    $parents[$item->getId()] = $level . $item->getTitle();
                }
                $this->getParents($Menu, $parents, $exclude, $item->getId(), $level . '- ');
            }
        }
    }
}